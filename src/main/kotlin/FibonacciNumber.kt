import java.util.*

fun main() {
    var a = 0
    var b = 1
    var listFab: List<Int> = listOf()

    for (i in 1..15) {
        listFab += a
        val sumOfPrevTwo = a + b
        a = b
        b = sumOfPrevTwo
    }

    // without input external
    val input = 5
    println(listFab.take(input))

    // with input external
    val reader = Scanner(System.`in`)
    print("Enter an input: ")
    val inputExternal = reader.nextInt()
    println(listFab.take(inputExternal))
}
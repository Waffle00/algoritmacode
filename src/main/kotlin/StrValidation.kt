fun main() {
    print("Enter an input: ")
    val input = readln()
    val grouping = input.groupBy { it }
    println(grouping.keys.size)
}